module.exports = {
  purge: [],
  theme: {





    extend: {
      colors: {
        navy: '#2f3458',
          primarydark: '#2a255e',
          primarylight: '#ffda00',
          secondarydark: '#ee8d7a',
          secondarylight: '#50b6b7',
          supportingdark: '#37373a',
          supportinglight: '#f4eff2',
          footerbackground: '#fff852',
          footertext: '#2a255e',
          footerhovertext: '#f34213',

      },

      fontFamily: {
            'Roboto': ['Roboto', 'sans-serif'],
            'MyFont': ['"My Font"', 'serif'] // Ensure fonts with spaces have " " surrounding it.
          },

    },
  },
  variants: {},
  plugins: [],
}
