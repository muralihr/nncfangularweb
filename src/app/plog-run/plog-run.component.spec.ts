import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlogRunComponent } from './plog-run.component';

describe('PlogRunComponent', () => {
  let component: PlogRunComponent;
  let fixture: ComponentFixture<PlogRunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlogRunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlogRunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
