import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NandiHabbaComponent } from './nandi-habba.component';

describe('NandiHabbaComponent', () => {
  let component: NandiHabbaComponent;
  let fixture: ComponentFixture<NandiHabbaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NandiHabbaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NandiHabbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
