import { Component } from '@angular/core';

import { NavbarComponent } from './navbar/navbar.component';
import { NavbarArrowComponent } from './navbar-arrow/navbar-arrow.component';

import { FooterComponent } from './footer/footer.component'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'NAMMA NIMMA CYCLE FOUNDATION ';
}
