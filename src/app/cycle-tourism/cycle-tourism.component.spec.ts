import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CycleTourismComponent } from './cycle-tourism.component';

describe('CycleTourismComponent', () => {
  let component: CycleTourismComponent;
  let fixture: ComponentFixture<CycleTourismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CycleTourismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CycleTourismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
