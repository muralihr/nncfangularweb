import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarArrowComponent } from './navbar-arrow.component';

describe('NavbarArrowComponent', () => {
  let component: NavbarArrowComponent;
  let fixture: ComponentFixture<NavbarArrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarArrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarArrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
