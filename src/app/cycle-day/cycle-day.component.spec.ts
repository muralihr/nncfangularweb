import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CycleDayComponent } from './cycle-day.component';

describe('CycleDayComponent', () => {
  let component: CycleDayComponent;
  let fixture: ComponentFixture<CycleDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CycleDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CycleDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
