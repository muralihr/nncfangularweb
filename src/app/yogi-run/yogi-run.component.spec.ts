import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YogiRunComponent } from './yogi-run.component';

describe('YogiRunComponent', () => {
  let component: YogiRunComponent;
  let fixture: ComponentFixture<YogiRunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YogiRunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YogiRunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
