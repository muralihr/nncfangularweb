import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrbanBicycleComponent } from './urban-bicycle.component';

describe('UrbanBicycleComponent', () => {
  let component: UrbanBicycleComponent;
  let fixture: ComponentFixture<UrbanBicycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrbanBicycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrbanBicycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
