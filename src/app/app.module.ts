import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ImgGalleryComponent } from './img-gallery/img-gallery.component';
import { AboutComponent } from './about/about.component';
import { VisionComponent } from './vision/vision.component';
import { AwardsComponent } from './awards/awards.component';
import { EventsComponent } from './events/events.component';
import { NewsComponent } from './news/news.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { PlogRunComponent } from './plog-run/plog-run.component';
import { NandiHabbaComponent } from './nandi-habba/nandi-habba.component';
import { YogiRunComponent } from './yogi-run/yogi-run.component';
import { CycleDayComponent } from './cycle-day/cycle-day.component';
import { CycleTourismComponent } from './cycle-tourism/cycle-tourism.component';
import { ServicesComponent } from './services/services.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BikeAnimationComponent } from './bike-animation/bike-animation.component';
import { FooterComponent } from './footer/footer.component';
import { UrbanBicycleComponent } from './urban-bicycle/urban-bicycle.component';
import { ContactComponent } from './contact/contact.component';
import { NavbarArrowComponent } from './navbar-arrow/navbar-arrow.component';
import { TeamComponent } from './team/team.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,

    ImgGalleryComponent,
    AboutComponent,
    VisionComponent,
    AwardsComponent,
    EventsComponent,
    NewsComponent,
    JoinUsComponent,
    PlogRunComponent,
    NandiHabbaComponent,
    YogiRunComponent,
    CycleDayComponent,
    CycleTourismComponent,
    ServicesComponent,
    NavbarComponent,
    BikeAnimationComponent,
    FooterComponent,

    UrbanBicycleComponent,

    ContactComponent,
    NavbarArrowComponent, 
    TeamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    AuthGuardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
