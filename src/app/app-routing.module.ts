import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';

import { ImgGalleryComponent } from './img-gallery/img-gallery.component';
import { AboutComponent } from './about/about.component';
import { VisionComponent } from './vision/vision.component';
import { AwardsComponent } from './awards/awards.component';
import { EventsComponent } from './events/events.component';
import { NewsComponent } from './news/news.component';
import { JoinUsComponent } from './join-us/join-us.component';


import { TeamComponent } from './team/team.component';

import { PlogRunComponent } from './plog-run/plog-run.component';
import { NandiHabbaComponent } from './nandi-habba/nandi-habba.component';
import { YogiRunComponent } from './yogi-run/yogi-run.component';
import { CycleDayComponent } from './cycle-day/cycle-day.component';
import { CycleTourismComponent } from './cycle-tourism/cycle-tourism.component';
import { UrbanBicycleComponent } from './urban-bicycle/urban-bicycle.component';
import { ContactComponent } from './contact/contact.component';
import { ServicesComponent } from './services/services.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'about', component: AboutComponent },
  { path: 'team', component: TeamComponent },


  { path: 'joinus', component: JoinUsComponent },
  { path: 'contact', component: ContactComponent },


  // All outreach
  { path: 'gallery', component: ImgGalleryComponent },
  { path: 'events', component: EventsComponent },
  { path: 'news', component: NewsComponent },
  //report and awards
  { path: 'awards', component: AwardsComponent },


  // All projects
  { path: 'nandi', component: NandiHabbaComponent },
  { path: 'yogirun', component: YogiRunComponent },
  { path: 'plog', component: PlogRunComponent },
  //  { path: 'ksrpkd', component: KSRPComponent  },

  ///All cycle work
  { path: 'urbancycle', component: UrbanBicycleComponent },
  { path: 'cycletourism', component: CycleTourismComponent },
  { path: 'cyclecooperatives', component: VisionComponent },
  { path: 'campaigns', component: CycleDayComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
